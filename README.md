# worldpipe

TeamCity Docker Builder

<div align="center">
    ![Screenshot, Worldpipe FirstStart](readme/screenshot-worldpipe-firststart-01.png)
</div>


## About

This repository provides a quick creation tool for an on-premises TeamCity docker deployment. It is a proof-of-concept used for pipeline and tools development.

## Background

For development and testing of Unreal Engine plugins and builds, I needed a quick and easy way to spin up/down multiple TeamCity servers with persistent storage and access to a Perforce HelixCore VCS. This repo is a tool to address that.

https://www.jetbrains.com/teamcity/

## Requirements

- [Docker](https://www.docker.com)
- [Make](https://www.gnu.org/software/make/)
- [Perforce](https://www.perforce.com/products/helix-core) server

See my [chiral](https://gitlab.com/halftonehell/chiral) repository for Perforce VCS deployment.

## Installation

_IMPORTANT_: This is an on-premises repo that uses self-signed certs for proof-of-concept. Do *NOT* put your real domain/production certs in git.

- Create persistent storage directories

```
❯ mkdir -p TeamCityDemo/data TeamCityDemo/logs TeamCityDemo/mariadb
```


- In *worldpipe/docker/dotenv*, set the desired bind root

```
TC_BIND_ROOT=/Path/to/Persistent/Storage/TeacmCityDemo
```

- In *worldpip/docker/env_mariadb*, set the desired MySQL DB parameters.

```
MARIADB_ROOT_PASSWORD=
MARIADB_DATABASE=
MARIADB_USER=
MARIADB_PASSWORD=
MARIADB_MYSQL_LOCALHOST_USER=1
```

- In *worldpipe/docker/tls*, replace the self-signed certs and _authorized_keys_ (for optional SSH) with your own. Also consider creating a new Java keystore, see *HOWTO_MAKE_JAVA_KEYSTORE.txt* and *conf/server.xml*.
- Build and start the container.

```
❯ cd worldpipe
❯ make -C docker P4PORT=ssl:11.22.33.44:1666 build
❯ make -C docker up
```

- To stop the container:

```
❯ make -C docker down
```

- At First Start, verify the data directory and click _Proceed_
- Select *MySQL* Database
- Download the JDBC and enter the MySQL DB connection settings
- Continue with installation as desired

## Defaults

| User      | Password  | Availability  |
| ---       | ---       | ---           |
| root      | d0ck3r    | shell         |
| foo       | P455w0rd  | shell         |

| Database         |          |
| ---              | ---      |
| Root password    | Ch4n63M3 |
| Database         | teamcity |
| DB User          | teamcity |
| DB User Password | T34mc1ty |
| DB port          |     3306 |

| Java keystore |          |
| ---           | ---      |
| password      | Ch4n63M3 |

## In Action

TeamCity has a great Unity pipeline video: [CI/CD for Unity Games Using TeamCity](https://www.youtube.com/watch?v=Sh_BRCw7prc). Adapt that information for an Unreal Project build. Its pretty straight-forward except you don't need a build file in the repo. You can simply build with a command line on the agent (see Unreal Editor's *Output Log* for the build command). See my [DeployAR](https://gitlab.com/halftonehell/deployar) project to see the example implementation.

```
cd /Users/Shared/Epic\ Games/UE_4.27/Engine/Build/BatchFiles \
&& ./RunUAT.sh -ScriptsForProject=/Volumes/RAVAGE/data/UnrealProjects/ARPluginSandbox/ARPluginSandbox.uproject BuildCookRun -nocompileeditor -installed -nop4 -project=/Volumes/RAVAGE/data/UnrealProjects/ARPluginSandbox/ARPluginSandbox.uproject -cook -stage -archive -archivedirectory=/Volumes/RAVAGE/data/UnrealProjects/ARPluginSandbox/Build -package -ue4exe=/Users/Shared/Epic\ Games/UE_4.27/Engine/Binaries/Mac/UE4Editor.app/Contents/MacOS/UE4Editor -compressed -ddc=InstalledDerivedDataBackendGraph -pak -prereqs -nodebuginfo -targetplatform=IOS -build -target=ARPluginSandbox -clientconfig=Development -utf8output -unattended
```

<div align="center">
![TeamCity Build Trigger Video](https://github.com/JustAddRobots/vault/assets/59129905/1ca1780b-2303-4c08-8ae7-4825b457d4da)
</div>


## TODO

- [ ] Add Slack messaging
- [ ] Add on-prem MariaDB storage/query of certs, public-keys

## Support

This proof-of-concept is an alpha, thus currently unsupported.

## License

Licensed under GNU GPL v3. See [LICENSE](LICENSE).
