Changelog
=========

0.2.0 (2023-06-10)
------------------
- ISSUE-008: Tweaked text, added Slack TODO. (6cdf9ce) [Roderick Constance]
- ISSUE-008: Added In Action video and build command line. (aed6950) [Roderick Constance]

0.1.0 (2023-06-03)
------------------
- ISSUE-006: Fixed Java keystore table. (2974184) [Roderick Constance]
- ISSUE-006: Rearranged container stop instructions. (5db4101) [Roderick Constance]
- ISSUE-006: Added Java keystore defaults. (e2e2578) [Roderick Constance]
- ISSUE-006: Added misc to README. (5645d45) [Roderick Constance]
- ISSUE-006: Fixed table, formatting. (38fcf2d) [Roderick Constance]
- ISSUE-006: Added README. (e777c4d) [Roderick Constance]
- ISSUE-005: Removed commented out JDBC bits. (92bfaeb) [Roderick Constance]
- ISSUE-005: Update mariadb pw, removed separate webserver service. (3f70770) [Roderick Constance]
- ISSUE-005: Disabled JDBC copy, disabled agent start. (6dea0b6) [Roderick Constance]
- ISSUE-005: Changed to user foo, added data path envar to compose. (741051c) [Roderick Constance]
- ISSUE-005: Updated to rocky-8.7, added dotenv. (59f7e3e) [Roderick Constance]
- ISSUE-005: Added parameterized P4PORT. (554c0e8) [Roderick Constance]
- ISSUE-004: Cleaned up config after working proof-of-concept. (c281d95) [Roderick Constance]
- ISSUE-004: Removed unused ansible bits. (ccd8080) [Roderick Constance]
- ISSUE-004: Added WIP for archive. (4cc3328) [Roderick Constance]
- ISSUE-004: Refreshed container image back to working order. (f424405) [Roderick Constance]
- ISSUE-004: added configure_mariadb, teamcity server start. (eed672c) [Roderick Constance]
- ISSUE-004: Added JRE fixes, updated to TeamCity 2022.10. (a939427) [Roderick Constance]
- ISSUE-004: Added MariaDB, env config updates. (3320c00) [Roderick Constance]
- ISSUE-004: Added MySQL config updates. (52c7d82) [Roderick Constance]
- ISSUE-004: Fixed java, teamcity tls for nginx proxy. (8ade065) [Roderick Constance]
- ISSUE-004: Added ansible and docker bits for build. (7f634f2) [Roderick Constance]
- ISSUE-001: Added basic repo bits. (967d665) [Roderick Constance]
- Initial commit. (5c99bba) [JustAddRobots]
