export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.18.0.10-2.el8_7.x86_64
export JRE_HOME=/usr/lib/jvm/java-11-openjdk-11.0.18.0.10-2.el8_7.x86_64
export TEAMCITY_JRE=/usr/lib/jvm/java-11-openjdk-11.0.18.0.10-2.el8_7.x86_64
export TEAMCITY_DATA_PATH=/opt/jetbrains/TeamCity/data
export TEAMCITY_LOGS_PATH=/opt/jetbrains/TeamCity/logs
export TEAMCITY_SERVER_MEM_OPTS=-Xmx2048m
