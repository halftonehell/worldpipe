FROM rockylinux:8.7

WORKDIR /
COPY ./repofile/* /etc/yum.repos.d/

ENV TZ=America/Los_Angeles
ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.18.0.10-2.el8_7.x86_64
ARG P4PORT="${P4PORT}"

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone \
    && dnf -y --disablerepo=* --enablerepo=vault-rocky* install \
    epel-release \
    && dnf -y --disablerepo=* --enablerepo=vault-rocky* install \
    git \
    java-11-openjdk-11.0.18.0.10 \
    nginx \
    openssh-clients \
    openssh-server \
    passwd \
	procps-ng \
    python3-pip \
    sudo \
    tree \
    vim \
    wget \
    && dnf clean all && rm -rf /var/cache/dnf \
    && ssh-keygen -t rsa -b 4096 -N '' -f ~/.ssh/id_rsa && ssh-keygen -A \
    && echo d0ck3r | passwd root --stdin \
    && useradd foo && echo P455w0rd | passwd foo --stdin \
    && usermod -a -G wheel foo \
    && mkdir /home/foo/.ssh \
    && ssh-keygen -t rsa -b 4096 -N '' -f /home/foo/.ssh/id_rsa \
    && rm -f /var/run/nologin \
    && alternatives --set java /usr/lib/jvm/java-11-openjdk-11.0.18.0.10-2.el8_7.x86_64/bin/java \
    && mkdir -p /opt/jetbrains \
    && wget -O /tmp/TeamCity.tar.gz https://download.jetbrains.com/teamcity/TeamCity-2022.10.3.tar.gz \
    && tar -C /opt/jetbrains -xzf /tmp/TeamCity.tar.gz \
    && rm -rf /tmp/TeamCity.tar.gz \
    && mkdir /opt/jetbrains/TeamCity/data \
    && mkdir /opt/jetbrains/TeamCity/logs \
    && mv /opt/jetbrains/TeamCity/webapps/ROOT /opt/jetbrains/TeamCity/webapps/teamcity \
    && mv /opt/jetbrains/TeamCity/conf/server.xml{,.ORIG} \
    && echo "teamcity.data.path=/opt/jetbrains/TeamCity/data" >> /opt/jetbrains/TeamCity/conf/teamcity-startup.properties

COPY ./tls/privatekey.txt /etc/pki/tls/private/
COPY ./tls/certificate.txt /etc/pki/tls/certs/
COPY ./tls/teamcity.jks /etc/pki/tls/
COPY ./tls/authorized_keys /root/.ssh/authorized_keys
COPY ./tls/authorized_keys /home/foo/.ssh/authorized_keys
COPY ./conf/nginx.conf /etc/nginx/nginx.conf
COPY ./conf/setenv.sh /etc/profile.d/
COPY ./conf/server.xml /opt/jetbrains/TeamCity/conf/

RUN echo "JAVA_HOME: $JAVA_HOME" \
    && chmod 600 /root/.ssh/authorized_keys \
    && chmod 600 /home/foo/.ssh/authorized_keys \
    && chown -R foo:foo /home/foo/.ssh

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone \
    && dnf -y install \
	helix-p4d \
    && dnf clean all && rm -rf /var/cache/dnf

RUN wget -P /tmp https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-8.0.31-1.el8.noarch.rpm \
    && dnf -y -q install /tmp/mysql-connector-j-8.0.31-1.el8.noarch.rpm

CMD /usr/sbin/sshd ; /usr/sbin/nginx ; /opt/jetbrains/TeamCity/bin/teamcity-server.sh start ; /usr/bin/bash
